export default class StorageService {
    static set(key, value) {
        localStorage.setItem(key, JSON.stringify(value));
    }

    static get(key) {
        let data = localStorage.getItem(key);
        if (data)
            return JSON.parse(data);
        else return null;
    }

    static del(key) {
        return localStorage.removeItem(key);
    }
}