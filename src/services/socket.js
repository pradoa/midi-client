import StorageService from '../services/storage';

export default class Socket {

    static Socket = null;

    static async Create() {
        if (this.Socket){
            this.Socket.close(1000);
        }

        let ip = "127.0.0.1";
        let configData = StorageService.get('config');
        if (configData){
            ip = configData.ip;
        }

        this.Socket = new WebSocket(`ws://${ip}:45133`)
    }

    static Send(type, value, control, channel) {
        if (this.Socket.readyState === 1) {
            this.Socket.send(`${type}:${value}:${control}:${channel}`)
        } else {
            let t = this
            setTimeout(function () {
                t.Send(type, value)
            }, 50)
        }
    }
}