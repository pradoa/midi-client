import StorageService from '../services/storage';

export default class ApiService {
    static API_URL(){
        let configData = StorageService.get('config');
        if (configData){
            return `http://${configData.ip}:8080`;
        }

        return 'http://127.0.0.1:8080';
    }

    static serialize = function (obj) {
        var str = [];
        for (var p in obj)
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        return str.join("&");
    }

    static async get(page, data) {
        return await fetch(`${ApiService.API_URL()}/${page}?${this.serialize(data)}`)
            .then(async (response) => {
                return await response.json()
            })
    }

    static async post(page, data) {
        return await fetch(`${ApiService.API_URL()}/${page}`, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(data)
        })
    }
}