import React, { Fragment } from 'react';
import Socket from '../services/socket';
import Api from '../services/api';
import { Modal, Button as BButton, ListGroup, ListGroupItem } from 'react-bootstrap';
import StorageService from '../services/storage';
import ConfigWindow from '../components/config';
import Controller from '../components/controller';
import Toast from 'light-toast';

export default class Main extends React.Component {
    constructor() {
        super();

        this.state = {
            data: false,
            ldata: false,
            showList: false,
            locked: true,
            menu: false,
        };

        this.saveData = this.saveData.bind(this);
        this.updateData = this.updateData.bind(this);
        this.showList = this.showList.bind(this);
    }

    async componentDidMount() {
        this.newConnection();
    }

    newConnection(){
        let configData = StorageService.get('config');
        if (configData){
            Socket.Create();
            this.loadList();
        }
    }

    async showList(showList) {
        await this.setState({ showList });
    }

    async loadList() {
        let ldata = await Api.get('list', {})
        this.setState({
            ldata: ldata,
        })
    }

    async loadData(name) {
        let cdata = await Api.get('load', { name: name })
        let ldata = await Api.get('list', {})
        this.setState({
            data: cdata,
            ldata: ldata,
            activeControl: 0,
            showList: false,
        })
    }

    async updateData(data) {
        this.setState({data});
    }

    async saveData(cdata) {
        Toast.loading('Salvando...');
        await Api.post('save', cdata);
        Toast.success("Salvo!", 2000, () => {});
    }

    async createNew() {
        let res = await Api.post('new', {});
        let name = await res.text();

        await this.loadList();
        this.loadData(name);
    }

    getPresetsList() {
        const { ldata, data } = this.state;
        if (ldata) {
            let list = Object.values(ldata).map((p, i) => {
                let name = p.split(`.json`)[0]
                let active = false
                if (data)
                    active = name.toLowerCase() === data.name.toLowerCase()
                return (
                    <ListGroupItem key={i} href="#" active={active} onClick={() => { this.loadData(name) }}>
                        {name}
                    </ListGroupItem>
                )
            })

            return (
                <ListGroup>
                    {list}
                </ListGroup>
            )
        }
    }

    getListModal() {
        return (
            <Modal show={this.state.showList} onHide={() => this.showList(false)}>
                <Modal.Header>
                    <Modal.Title>Carregar Preset</Modal.Title>
                </Modal.Header>

                <Modal.Body>{this.getPresetsList()}</Modal.Body>

                <Modal.Footer>
                    <BButton variant="primary" onClick={this.createNew.bind(this)}>Criar Novo Preset</BButton>
                    <BButton onClick={() => this.showList(false)}>Cancelar</BButton>
                </Modal.Footer>
            </Modal>
        )
    }

    getController() {
        const { data } = this.state;
        let configData = StorageService.get('config');
        if (configData && data) {
            return (
                <div id="main">
                    {this.getListModal()}
                    <Controller data={data} onUpdate={this.updateData} onSave={this.saveData} showList={this.showList} />
                </div>
            )
        } else {
            return this.renderHello();
        }
    }

    renderHello(){
        const { ldata } = this.state;
        return (
            <div className="flex-container full-height">
                <h1>WiFi Midi Controller</h1>
                {this.getListModal()}
                {
                    ldata && ldata.length ? (
                        <button className="btn btn-default" onClick={() => this.showList(true)}>
                            Carregar Preset
                        </button>
                    ) : (null)
                }
            </div>
        );
    }

    renderConfigWindow(){
        return (
            <ConfigWindow onSave={() => this.newConnection()}/>
        );
    }

    render() {
        return (
            <Fragment>
                {this.renderConfigWindow()}
                {this.getController()}
            </Fragment>
        );
    }
}