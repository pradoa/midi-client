import React, { Fragment } from 'react';
import { Modal, Button, Form, Label, FormControl } from 'react-bootstrap';
import StorageService from '../../services/storage';
import Toast from 'light-toast';

export default class ConfigWindow extends React.Component {
    constructor() {
        super();

        let config = StorageService.get('config');
        this.state = {
            showWindow: false,
            formData: config ? config : {
                ip: ""
            },
        };

        this.updateFormData = this.updateFormData.bind(this);
        this.save = this.save.bind(this);
    }

    async showModal(showWindow) {
        await this.setState({ showWindow });
    }

    updateFormData(key, val){
        let { formData } = this.state;
        formData[key] = val;
        this.setState({formData});
    }

    save(){
        const { formData } = this.state;
        if (formData.ip){
            StorageService.set("config", formData);
            this.props.onSave();
            Toast.success("Salvo!", 2000, () => {});
        }
    }

    getModal() {
        const { showWindow, formData } = this.state;
        return (
            <Modal show={showWindow} onHide={() => this.showModal(false)} backdrop="static">
                <Modal.Header closeButton>
                    <Modal.Title>Configurar</Modal.Title>
                </Modal.Header>

                <Modal.Body>
                    <Form>
                        <Label>IP do Servidor</Label>
                        <FormControl type="text" placeholder="IP" value={formData.ip} onChange={(e) => this.updateFormData('ip', e.target.value)} />
                    </Form>
                </Modal.Body>

                <Modal.Footer>
                    <Button variant="primary" onClick={() => this.save()}>Salvar</Button>
                </Modal.Footer>
            </Modal>
        )
    }


    render(){
        return (
            <Fragment>
                {this.getModal()}
                <div className="config-btn-wrapper">
                    <Button variant="primary" onClick={() => this.showModal(true)}><i className="material-icons">settings</i></Button>
                </div>
            </Fragment>
        )
    }
}