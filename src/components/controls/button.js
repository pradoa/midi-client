import React from 'react'
import Socket from '../../services/socket'

export default class Button extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            locked: this.props.locked ? this.props.locked : false,
            owner: this.props.owner ? this.props.owner : false,

            id: this.props.id || this.props.id === 0 ? this.props.id : null,
            className: this.props.className ? this.props.className : null,
            children: this.props.children ? this.props.children : null,
            type: this.props.type ? this.props.type : false,
            control: this.props.control ? this.props.control : false,
            value: this.props.value ? this.props.value : false,
            channel: this.props.channel ? this.props.channel : false,

            width: this.props.width ? Number.parseInt(this.props.width, 10) : false,
            height: this.props.height ? Number.parseInt(this.props.height, 10) : false,
            top: this.props.top ? Number.parseInt(this.props.top, 10) : false,
            left: this.props.left ? Number.parseInt(this.props.left, 10) : false,
            background: this.props.background ? this.props.background : `#fff`,
            color: this.props.color ? this.props.color : false,

            onClick: this.props.onClick ? this.props.onClick : false,

            draggable: false,
            cc: false,
            mt: 0,
            ml: 0,
            ct: 0,
            cl: 0,
            tmv: 0,


        }

        let bt = this
        document.addEventListener('mousemove', function (e) {
            let mx = e.clientX
            let my = e.clientY

            bt.setState({
                mt: my,
                ml: mx
            })
        }, true);
        document.addEventListener('touchmove', function (e) {
            let mx = e.touches[0].clientX
            let my = e.touches[0].clientY

            bt.setState({
                mt: my,
                ml: mx
            })
        }, true);

        this.click = this.click.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            locked: props.locked ? props.locked : false,

            id: props.id || props.id === 0 ? props.id : null,
            className: props.className ? props.className : null,
            children: props.children ? props.children : null,
            type: props.type ? props.type : false,
            control: props.control ? props.control : false,
            value: props.value ? props.value : false,
            channel: props.channel ? props.channel : false,

            width: props.width ? Number.parseInt(props.width, 10) : 0,
            height: props.height ? Number.parseInt(props.height, 10) : 0,
            top: props.top ? Number.parseInt(props.top, 10) : 0,
            left: props.left ? Number.parseInt(props.left, 10) : 0,
            background: props.background ? props.background : "#fff",
            color: props.color ? props.color : false,

            onClick: props.onClick ? props.onClick : false,
        })
    }

    click(evt) {
        if (this.state.locked) {
            let s = this.state
            if (s.type && s.control && s.value && s.channel) {
                Socket.Send(s.type, s.value, s.control, s.channel)
            }
        }

        if (this.state.onClick)
            this.state.onClick(this, evt)

    }

    mouseDown(el) {
        let owner = this.state.owner
        if (owner) {
            owner.setState({
                activeControl: this.state.id
            })
        }

        let c = el.currentTarget
        let t = c.offsetTop
        let l = c.offsetLeft

        let b = document.getElementById('controller')
        let bt = b.offsetTop
        let bl = b.offsetLeft

        let mt = this.state.mt - bt
        let ml = this.state.ml - bl

        let lt = mt - t
        let ll = ml - l

        if (lt < 0)
            lt = 0
        if (ll < 0)
            ll = 0

        this.setState({
            cc: b,
            ct: lt,
            cl: ll,
            draggable: true,
        })
    }

    mouseUp() {
        this.setState({
            draggable: false,
            tmv: 0,
        })
    }

    mouseMove(el) {
        if (this.state.draggable && this.state.cc && !this.state.locked) {
            let cct = this.state.ct
            let ccl = this.state.cl

            let owner = this.state.owner
            let b = this.state.cc
            let bt = b.offsetTop
            let bl = b.offsetLeft

            let mt = this.state.mt - bt
            let ml = this.state.ml - bl

            let ct = mt - cct
            let cl = ml - ccl

            if (ct < 0) { ct = 0 }
            if (cl < 0) { cl = 0 }

            // grade of Xpx
            ct = this.getRound(ct, 10)
            cl = this.getRound(cl, 10)

            this.setState({
                top: ct,
                left: cl
            })

            if (owner) {
                owner.changeControlProperty(owner.state.activeControl, 'top', ct)
                owner.changeControlProperty(owner.state.activeControl, 'left', cl)
            }
        }
    }

    getRound(num, grade) {
        if (num === 0 || grade === 0)
            return 0
        //131 20
        console.log(num, grade);
        let m = 1
        while (grade * m < num) {
            m++
        }

        let gnum = grade * m // 7 = 140
        let hg = grade / 2

        if (gnum - (hg + 1) > num) {
            return grade * (m - 1)
        } else {
            return gnum
        }

    }

    touchMove(el) {
        if (this.state.tmv <= 0) {
            let ctmv = this.state.tmv + 1
            this.mouseDown(el)
            this.setState({
                tmv: ctmv
            })
        }
        if (this.state.draggable && this.state.cc && !this.state.locked) {
            let cct = this.state.ct
            let ccl = this.state.cl

            let owner = this.state.owner

            let c = el.currentTarget
            let t = c.offsetTop
            let l = c.offsetLeft

            let b = document.getElementById('controller')
            let bt = b.offsetTop
            let bl = b.offsetLeft

            let mt = this.state.mt - bt
            let ml = this.state.ml - bl

            let ct = mt - cct
            let cl = ml - ccl

            // grade of Xpx
            ct = this.getRound(ct, 10)
            cl = this.getRound(cl, 10)
            //console.log(ct, cl)

            this.setState({
                top: ct,
                left: cl,
            })

            if (owner) {
                owner.changeControlProperty(owner.state.activeControl, 'top', ct)
                owner.changeControlProperty(owner.state.activeControl, 'left', cl)
            }
        }
    }

    render() {
        return (
            <button
                id={this.state.id ? this.state.id : undefined}
                className={this.state.className}
                onClick={this.click}
                onMouseDown={(el) => { this.mouseDown(el) }}
                onMouseUp={(el) => { this.mouseUp(el) }}
                onMouseMove={(el) => { this.mouseMove(el) }}

                onTouchStart={(el) => { this.mouseDown(el) }}
                onTouchEnd={(el) => { this.mouseUp(el) }}
                onTouchMove={(el) => { this.touchMove(el) }}
                style={{
                    position: `absolute`,
                    width: this.state.width ? this.state.width : 50,
                    height: this.state.height ? this.state.height : 50,
                    top: this.state.top ? this.state.top : false,
                    left: this.state.left ? this.state.left : false,
                    background: this.state.background ? this.state.background : `transparent`,
                    color: this.state.color ? this.state.color : false,
                    overflow: `hidden`,
                    wordBreak: `break-all`,
                }}>
                {this.state.children}
            </button>
        )
    }
}