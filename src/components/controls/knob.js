import React from 'react';
import Socket from '../../services/socket';
import MSlider from '@material-ui/core/Slider';

export default class Knob extends React.Component {
    compare_value = 0;

    constructor(props) {
        super(props)
        this.state = {
            locked: this.props.locked ? this.props.locked : false,
            owner: this.props.owner ? this.props.owner : false,

            id: this.props.id || this.props.id === 0 ? this.props.id : null,
            className: this.props.className ? this.props.className : null,
            children: this.props.children ? this.props.children : null,
            type: this.props.type ? this.props.type : false,
            control: this.props.control ? this.props.control : false,
            value: this.props.value ? this.props.value : false,
            channel: this.props.channel ? this.props.channel : false,

            width: this.props.width ? Number.parseInt(this.props.width, 10) : false,
            height: this.props.height ? Number.parseInt(this.props.height, 10) : false,
            top: this.props.top ? Number.parseInt(this.props.top, 10) : false,
            left: this.props.left ? Number.parseInt(this.props.left, 10) : false,
            background: this.props.background ? this.props.background : `#fff`,
            color: this.props.color ? this.props.color : false,

            onClick: this.props.onClick ? this.props.onClick : false,
            onChange: this.props.onChange ? this.props.onChange : false,
            zIndex: 2,

            draggable: false,
            cc: false,
            mt: 0,
            ml: 0,
            ct: 0,
            cl: 0,
            tmv: 0,

            sliderTop: 0,
            sliderLeft: 0,
        }

        let bt = this
        document.addEventListener('mousemove', function (e) {
            let mx = e.clientX
            let my = e.clientY

            bt.setState({
                mt: my,
                ml: mx
            })
        }, true);
        document.addEventListener('touchmove', function (e) {
            let mx = e.touches[0].clientX
            let my = e.touches[0].clientY

            bt.setState({
                mt: my,
                ml: mx
            })
        }, true);

        this.click = this.click.bind(this);
        this.change = this.change.bind(this);
    }

    componentWillReceiveProps(props) {
        this.setState({
            locked: props.locked ? props.locked : false,

            id: props.id || props.id === 0 ? props.id : null,
            className: props.className ? props.className : null,
            children: props.children ? props.children : null,
            type: props.type ? props.type : false,
            control: props.control ? props.control : false,
            value: props.value ? props.value : false,
            channel: props.channel ? props.channel : false,

            width: props.width ? Number.parseInt(props.width, 10) : 0,
            height: props.height ? Number.parseInt(props.height, 10) : 0,
            top: props.top ? Number.parseInt(props.top, 10) : 0,
            left: props.left ? Number.parseInt(props.left, 10) : 0,
            background: props.background ? props.background : "#fff",
            color: props.color ? props.color : false,

            onClick: props.onClick ? props.onClick : false,
            onChange: props.onChange ? props.onChange : false,
        })
    }

    click(evt) {
        if (this.state.locked) {
        }

        if (this.state.onClick)
            this.state.onClick(this, evt)
    }

    change(evt, val) {
        if (this.state.locked) {
            if (!this.state.draggable){
                this.compare_value = val;
                console.log('set', this.compare_value);
                return;
            }
            let addVal = (this.compare_value - val) * -1;
            let newVal = this.props.value + addVal;
            console.log(this.compare_value, val, addVal, newVal);

            if (newVal > 127){
                let diff = newVal - 127;
                this.compare_value += diff;
                newVal = 127;
            }
            if (newVal < 0){
                let diff = newVal * -1;
                this.compare_value -= diff;
                newVal = 0;
            }

            if (this.state.onChange)
                this.state.onChange(this, newVal)

            let s = this.state
            if (s.type && s.control && newVal && s.channel) {
                Socket.Send(s.type, newVal, s.control, s.channel)
            }
        }
    }

    mouseDown(el) {
        let owner = this.state.owner
        if (owner) {
            owner.setState({
                activeControl: this.state.id
            })
        }

        let c = el.currentTarget
        let t = c.offsetTop
        let l = c.offsetLeft

        let b = document.getElementById('controller')
        let bt = b.offsetTop
        let bl = b.offsetLeft

        let mt = this.state.mt - bt
        let ml = this.state.ml - bl

        let lt = mt - t
        let ll = ml - l

        if (lt < 0)
            lt = 0
        if (ll < 0)
            ll = 0

        this.setState({
            zIndex: 12,
            cc: b,
            ct: lt,
            cl: ll,
            draggable: true,
        })
    }

    mouseUp() {
        this.setState({
            draggable: false,
            zIndex: 2,
            tmv: 0,
        })
    }

    mouseMove(el) {
        if (this.state.draggable && this.state.cc && !this.state.locked) {
            let cct = this.state.ct
            let ccl = this.state.cl

            let owner = this.state.owner
            let b = this.state.cc
            let bt = b.offsetTop
            let bl = b.offsetLeft

            let mt = this.state.mt - bt
            let ml = this.state.ml - bl

            let ct = mt - cct
            let cl = ml - ccl

            if (ct < 0) { ct = 0 }
            if (cl < 0) { cl = 0 }

            // grade of Xpx
            ct = this.getRound(ct, 50)
            cl = this.getRound(cl, 50)

            this.setState({
                top: ct,
                left: cl
            })

            if (owner) {
                owner.changeControlProperty(owner.state.activeControl, 'top', ct)
                owner.changeControlProperty(owner.state.activeControl, 'left', cl)
            }
        }
    }

    getRound(num, grade) {
        if (num === 0 || grade === 0)
            return 0
        //131 20
        let m = 1
        while (grade * m < num) {
            m++
        }

        let gnum = grade * m // 7 = 140
        let hg = grade / 2

        if (gnum - (hg + 1) > num) {
            return grade * (m - 1)
        } else {
            return gnum
        }

    }

    touchMove(el) {
        if (this.state.tmv <= 0) {
            let ctmv = this.state.tmv + 1
            this.mouseDown(el)
            this.setState({
                tmv: ctmv
            })
        }
        if (this.state.draggable && this.state.cc && !this.state.locked) {
            let cct = this.state.ct
            let ccl = this.state.cl

            let owner = this.state.owner

            let c = el.currentTarget
            let t = c.offsetTop
            let l = c.offsetLeft

            let b = document.getElementById('controller')
            let bt = b.offsetTop
            let bl = b.offsetLeft

            let mt = this.state.mt - bt
            let ml = this.state.ml - bl

            let ct = mt - cct
            let cl = ml - ccl

            // grade of Xpx
            ct = this.getRound(ct, 50)
            cl = this.getRound(cl, 50)
            //console.log(ct, cl)

            this.setState({
                top: ct,
                left: cl,
            })

            if (owner) {
                owner.changeControlProperty(owner.state.activeControl, 'top', ct)
                owner.changeControlProperty(owner.state.activeControl, 'left', cl)
            }
        }
    }

    getAngle(){
        const { value } = this.props;
        let max = 150;

        let x = ((value * max * 2) / 127) - max;
        return x;
    }

    render() {
        const { sliderTop, sliderLeft } = this.state;

        return (
            <div
                style={{
                    position: `absolute`,
                    width: this.state.width ? this.state.width : 40,
                    height: this.state.height ? this.state.height : 30,
                    top: this.state.top ? this.state.top : false,
                    left: this.state.left ? this.state.left : false,
                    color: this.state.color ? this.state.color : false,
                    overflow: `hidden`,
                    wordBreak: `break-all`,
                }}
                className={this.state.className}
            >
                <div
                    className="knob-control"
                    onClick={this.click}
                    onMouseDown={(el) => { this.mouseDown(el) }}
                    onMouseUp={(el) => { this.mouseUp(el) }}
                    onMouseMove={(el) => { this.mouseMove(el) }}
    
                    onTouchStart={(el) => { this.mouseDown(el) }}
                    onTouchEnd={(el) => { this.mouseUp(el) }}
                    onTouchMove={(el) => { this.touchMove(el) }}
                    style={{
                        position: "absolute",
                        width: this.state.width && this.state.height ? (this.state.width > this.state.height ? this.state.height : this.state.width) : 40,
                        height: this.state.width && this.state.height ? (this.state.width > this.state.height ? this.state.height : this.state.width) : 40,
                        top: this.state.width && this.state.height ? (this.state.width > this.state.height ? 0 : (this.state.height - this.state.width) / 2) : 0,
                        left: this.state.width && this.state.height ? (this.state.width > this.state.height ? ((this.state.width - this.state.height) / 2) : 0) : 0,
                        backgroundColor: this.state.background ? this.state.background : false,
                        overflow: `hidden`,
                        wordBreak: `break-all`,
                        borderRadius: "50%",
                        border: "2px #444 solid",
                        transform: `rotate(${this.getAngle()}deg)`,
                        transition: "all 0.25s ease"
                    }}>
                    <div className="knob-control-indicator"
                        style={{
                            width: 10,
                            height: 10,
                            backgroundColor: this.state.color ? this.state.color : "#444",
                            borderRadius: "50%",
                            position: "absolute",
                            top: "calc(0% + 5px)",
                            left: "calc(50% - 5px)",
                        }}>
                    </div>
                </div>
                
                <MSlider
                    id={this.state.id ? `slider${this.state.id}` : undefined}
                    className={this.state.className }
                    aria-labelledby="continuous-slider"
                    orientation="vertical"

                    min={-127}
                    max={127}

                    onClick={this.click}
                    onMouseDown={(el) => { this.mouseDown(el) }}
                    onMouseUp={(el) => { this.mouseUp(el) }}
                    onMouseMove={(el) => { this.mouseMove(el) }}

                    onTouchStart={(el) => { this.mouseDown(el) }}
                    onTouchEnd={(el) => { this.mouseUp(el) }}
                    onTouchMove={(el) => { this.touchMove(el) }}

                    onChange={this.change}
                    defaultValue={0}

                    style={{
                        position: `absolute`,
                        height: this.state.height ? this.state.height * 3 : 50,
                        width: this.state.width ? this.state.width : 50,
                        top: sliderTop - this.state.height,
                        left: sliderLeft,
                        color: this.state.color ? this.state.color : false,
                        padding: 0,
                        opacity: 1,
                    }}
                />
                <style>
                    {`
                        #slider${this.state.id}.midi-control-slider .MuiSlider-track {
                            background-color: ${this.state.background ? this.state.background : `transparent`}
                        }
                        #slider${this.state.id}.midi-control-slider .MuiSlider-thumb {
                            background-color: ${this.state.color ? this.state.color : `transparent`}
                        }
                    `}
                </style>
            </div>
        )
    }
}