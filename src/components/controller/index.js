import React from 'react';
import Socket from '../../services/socket';
import CButton from '../controls/button';
import CSlider from '../controls/slider';
import CKnob from '../controls/knob';
import CSwitch from '../controls/switch';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

export default class Controller extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            data: props.data ? props.data : false,
            ldata: false,
            showList: true,
            locked: true,
            menu: false,
            showRemoveDialog: false,
        };

        Socket.Create();
        this.setActiveControl = this.setActiveControl.bind(this);
        this.toggleMenu = this.toggleMenu.bind(this);
        this.toggleLock = this.toggleLock.bind(this);
        this.save = this.save.bind(this);
    }

    componentWillReceiveProps(props){
        this.setState({
            data: props.data ? props.data : false,
        });
    }

    save(){
        const { data } = this.state;
        this.props.onSave(data);
    }

    showRemoveDialog(showRemoveDialog){
        this.setState({showRemoveDialog});
    }

    async toggleLock() {
        let lock = true
        if (this.state.locked)
            lock = false

        this.setState({
            locked: lock
        })
    }

    async toggleMenu() {
        let open = true
        if (this.state.menu)
            open = false

        this.setState({
            menu: open
        })
    }


    setActiveControl(el, evt) {
        let id = null;

        if (el) {
            id = el.props.id;
        }

        if (evt) {
            evt.stopPropagation();
        }

        this.setState({
            activeControl: id
        });

        return true;
    }

    toggleValue(el) {
        let { locked, data } = this.state;
        this.setActiveControl(el);

        if (!locked) return;

        let id = el.props.id
        let value = el.props.value

        if (value < 64)
            value = 65
        else
            value = 0

        let controls = data.controls
        let c = controls[id]
        c['midi_value'] = value

        this.setState({
            data: data
        })
    }

    changeControlValue(el, value) {
        let id = el.props.id

        if (value < 0)
            value = 0

        let data = this.state.data
        let controls = data.controls
        let c = controls[id]
        c['midi_value'] = value

        this.setState({
            data: data
        })
    }

    changeControlProperty(id, prop, value) {
        let data = this.state.data
        let controls = data.controls
        let c = controls[id]
        
        if (id >= 0 && c){
            c[prop] = value

            this.setState({
                data: data
            })
        }
    }

    changeProperty(prop, value) {
        let data = this.state.data
        data[prop] = value

        this.setState({
            data: data
        })
    }

    getActiveControlForm() {
        const { data, activeControl, showRemoveDialog } = this.state;
        let c = data.controls[activeControl];
        if (c) {

            let ctype = (
                <select className="form-control" name="ctype" value={c.midi_type}
                    onChange={(el) => { this.changeControlProperty(activeControl, 'midi_type', el.currentTarget.value) }}>
                    <option value="note">Nota On</option>
                    <option value="noteoff">Nota Off</option>
                    <option value="control">Controle</option>
                    <option value="switch">Switch</option>
                </select>
            )

            let ccont = (<input className="form-control" type="number" value={c.midi_control} onChange={(el) => { this.changeControlProperty(activeControl, 'midi_control', el.currentTarget.value) }} />)
            let cval = (<input className="form-control" type="number" value={c.midi_value} onChange={(el) => { this.changeControlProperty(activeControl, 'midi_value', el.currentTarget.value) }} />)
            let cch = (<input className="form-control" type="number" value={c.midi_channel} onChange={(el) => { this.changeControlProperty(activeControl, 'midi_channel', el.currentTarget.value) }} />)

            let cname = (<input className="form-control" type="text" value={c.name} onChange={(el) => { this.changeControlProperty(activeControl, 'name', el.currentTarget.value) }} />)
            let cbg = (<input className="form-control" type="text" value={c.background} onChange={(el) => { this.changeControlProperty(activeControl, 'background', el.currentTarget.value) }} />)
            let cw = (<input className="form-control" type="number" value={c.width} onChange={(el) => { this.changeControlProperty(activeControl, 'width', el.currentTarget.value) }} />)
            let ch = (<input className="form-control" type="number" value={c.height} onChange={(el) => { this.changeControlProperty(activeControl, 'height', el.currentTarget.value) }} />)
            let cleft = (<input className="form-control" type="number" value={c.left} onChange={(el) => { this.changeControlProperty(activeControl, 'left', el.currentTarget.value) }} />)
            let ctop = (<input className="form-control" type="number" value={c.top} onChange={(el) => { this.changeControlProperty(activeControl, 'top', el.currentTarget.value) }} />)
            let ccolor = (<input className="form-control" type="text" value={c.color} onChange={(el) => { this.changeControlProperty(activeControl, 'color', el.currentTarget.value) }} />)

            return (
                <div className="row">
                    <div className="col-xs-12">
                        <div className="form-group">
                            <h3>Controle {activeControl}</h3>
                        </div>

                        <h4>Midi</h4>
                        <div className="form-group">
                            <div className="form-group">
                                <label>Tipo:</label>
                                {ctype}
                            </div>

                            <div className="row">
                                <div className="col-xs-6">
                                    <div className="form-group">
                                        <label>Id Controle:</label>
                                        {ccont}
                                    </div>
                                </div>
                                <div className="col-xs-6">
                                    {
                                        c.type !== "switch" && c.type !== "slider" ? (
                                            <div className="form-group">
                                                <label>Valor de Envio:</label>
                                                {cval}
                                            </div>
                                        ): (null)
                                    }
                                </div>
                                <div className="col-xs-12">
                                    <div className="form-group">
                                        <label>Canal:</label>
                                        {cch}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h4>Aparência</h4>

                        <div className="form-group">
                            <label>Nome</label>
                            {cname}
                        </div>
                        <div className="form-group">
                            <label>Background</label>
                            {cbg}
                        </div>
                        <div className="form-group">
                            <label>Cor do Texto</label>
                            {ccolor}
                        </div>
                        <div className="form-group">
                            <label>Tamanho</label>
                            <div className="row">
                                <div className="col-xs-6">
                                    {cw}
                                </div>
                                <div className="col-xs-6">
                                    {ch}
                                </div>
                            </div>
                        </div>
                        <div className="form-group">
                            <label>Posição</label>
                            <div className="row">
                                <div className="col-xs-6">
                                    {ctop}
                                </div>
                                <div className="col-xs-6">
                                    {cleft}
                                </div>
                            </div>
                        </div>

                        <div className="form-group">
                            <button className="btn btn-danger btn-block" onClick={() => this.showRemoveDialog(true)}>Remover Controle</button>
                            <Dialog
                                open={showRemoveDialog}
                                onClose={() => this.showRemoveDialog(false)}
                                aria-labelledby="alert-dialog-title"
                                aria-describedby="alert-dialog-description"
                            >
                                <DialogTitle id="alert-dialog-title">Deseja remover o controle {activeControl}?</DialogTitle>
                                <DialogContent>
                                    <DialogContentText id="alert-dialog-description">
                                        Confirmar remoção?
                                    </DialogContentText>
                                </DialogContent>
                                <DialogActions>
                                    <Button onClick={() => this.removeControl(activeControl)} color="secondary" autoFocus>
                                        Confirmar
                                    </Button>
                                    <Button onClick={() => this.showRemoveDialog(false)} color="primary">
                                        Cancelar
                                    </Button>
                                </DialogActions>
                            </Dialog>
                        </div>
                    </div>
                </div>
            );
        }

        return (null);
    }

    getSidebar() {
        const { locked, data } = this.state;
        
        return (
            <div id="sidebar">
                <h4>Preset</h4>

                <div className="form-group sidebar-btn-group">
                    <button className="btn btn-primary" onClick={this.save}>
                        <i className="material-icons">
                            save
                        </i>
                    </button>
                    <button className="btn btn-primary" onClick={() => this.props.showList(true)}>
                        <i className="material-icons">
                            sync
                        </i>
                    </button>
                    <button className={`btn ${locked ? `btn-primary` : `btn-warning`}`} onClick={this.toggleLock}>
                        <i className={`material-icons`}>
                            {locked ? `lock` : `lock_open`}
                        </i>
                    </button>
                    <button className="btn btn-default btn-menu-close" onClick={this.toggleMenu}>
                        <i className="material-icons">
                            close
                        </i>
                    </button>
                </div>

                <label>Nome do Preset:</label>
                <div className="form-group">
                    <input className="form-control" id="presetName" name="presetName" value={data.name} onChange={(el) => { this.changeProperty('name', el.currentTarget.value) }} />
                </div>

                <label>Background do Preset:</label>
                <div className="form-group">
                    <input className="form-control" id="presetBg" name="presetBg" value={data.background} onChange={(el) => { this.changeProperty('background', el.currentTarget.value) }} />
                </div>

                {this.getActiveControlForm()}


                <div className="form-group sidebar-btn-group-low">
                    <label>Adicionar Controle:</label>
                    <button className="btn btn-primary btn-block" onClick={() => this.addControl('button')}>Botão</button>
                    <button className="btn btn-primary btn-block" onClick={() => this.addControl('switch')}>Switch</button>
                    <button className="btn btn-primary btn-block" onClick={() => this.addControl('slider')}>Slider</button>
                    <button className="btn btn-primary btn-block" onClick={() => this.addControl('knob')}>Knob</button>
                </div>
            </div>
        );
    }

    removeControl(id){
        let { data } = this.state;
        let controls = data.controls;
        if (id >= 0){
            controls.splice(id, 1);

            this.setState({
                data: data
            })

            this.showRemoveDialog(false);
        }
    }

    addControl(type){
        let { data } = this.state;
        let controls = data.controls;

        switch (type){
            case "button": {
                let control = {
                    "type": "button",
                    "width": 60,
                    "height": 60,
                    "left": 90,
                    "top": 90,
                    "name": "Button",
                    "midi_type": "note",
                    "midi_channel": 1,
                    "midi_control": 1,
                    "midi_value": 127,
                    "background": "#fff",
                    "color": "#000"
                };
                controls.push(control);
                break;
            }
            case "slider": {
                let control = {
                    "type": "slider",
                    "width": 30,
                    "height": 180,
                    "left": 90,
                    "top": 90,
                    "name": "Button",
                    "midi_type": "control",
                    "midi_channel": 1,
                    "midi_control": 1,
                    "midi_value": 65,
                    "background": "#0b4",
                    "color": "#fff"
                }
                controls.push(control);
                break;
            }
            case "switch": {
                let control = {
                    "type": "switch",
                    "width": 120,
                    "height": 60,
                    "left": 90,
                    "top": 90,
                    "name": "Slider",
                    "midi_type": "switch",
                    "midi_channel": 1,
                    "midi_control": 1,
                    "midi_value": 65,
                    "background": "#0b4",
                    "color": "#fff"
                }
                controls.push(control);
                break;
            }
        }

        this.setState({
            data: data
        });
    }

    getControls(controls) {
        let cts = Object.values(controls).map((c, i) => {
            switch (c.type) {
                case "button": return (
                    <CButton
                        key={i}
                        id={0 + i}
                        owner={this}
                        locked={this.state.locked}
                        className="midi-control-btn"

                        type={c.midi_type}
                        control={c.midi_control}
                        value={c.midi_value}
                        velocity={c.midi_velocity}
                        channel={c.midi_channel}

                        left={c.left}
                        top={c.top}
                        width={c.width}
                        height={c.height}
                        background={c.background}
                        color={c.color}

                        onClick={(el, evt) => this.setActiveControl(el, evt)}
                    >{c.name}</CButton>)

                case "slider":
                    return (
                        <CSlider
                            key={i}
                            id={0 + i}
                            owner={this}
                            locked={this.state.locked}
                            className="midi-control-slider"

                            type={c.midi_type}
                            control={c.midi_control}
                            value={c.midi_value}
                            velocity={c.midi_velocity}
                            channel={c.midi_channel}

                            left={c.left}
                            top={c.top}
                            width={c.width}
                            height={c.height}
                            background={c.background}
                            color={c.color}

                            onClick={this.setActiveControl.bind(this)}
                            onChange={this.changeControlValue.bind(this)}
                        ></CSlider>
                    )

                case "knob":
                    return (
                        <CKnob
                            key={i}
                            id={0 + i}
                            owner={this}
                            locked={this.state.locked}
                            className="midi-control-knob"

                            type={c.midi_type}
                            control={c.midi_control}
                            value={c.midi_value}
                            velocity={c.midi_velocity}
                            channel={c.midi_channel}

                            left={c.left}
                            top={c.top}
                            width={c.width}
                            height={c.height}
                            background={c.background}
                            color={c.color}

                            onClick={this.setActiveControl.bind(this)}
                            onChange={this.changeControlValue.bind(this)}
                        ></CKnob>
                    )

                case "switch":
                    return (
                        <CSwitch
                            key={i}
                            id={0 + i}
                            owner={this}
                            locked={this.state.locked}
                            className="midi-control-switch"

                            type={c.midi_type}
                            control={c.midi_control}
                            value={c.midi_value}
                            velocity={c.midi_velocity}
                            channel={c.midi_channel}

                            left={c.left}
                            top={c.top}
                            width={c.width}
                            height={c.height}
                            background={c.background}
                            color={c.color}

                            onClick={this.toggleValue.bind(this)}
                        >{c.name}</CSwitch>
                    )

                default:
                    return (<div key={i}>{c.name}</div>)
            }
        })

        return cts
    }

    render(){
        const { data, menu, locked } = this.state;
        return (
            <div
                id="controller"
                className={`${menu ? `menu-in` : ``}`}
                style={{ background: data.background }}
            >
                <div id="backdrop" style={{ backgroundImage: !locked ? "url(img/grid.png)" : "" }} onClick={(e) => this.setActiveControl(null, e)}></div>

                <button className="btn btn-menu btn-default" onClick={this.toggleMenu}>
                    <i className="material-icons">menu</i>
                </button>

                {this.getSidebar()}
                {this.getControls(data.controls)}
            </div>
        );
    }

}